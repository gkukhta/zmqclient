TEMPLATE = app
CONFIG += c++17 console link_pkgconfig
CONFIG -= app_bundle
CONFIG -= qt
PKGCONFIG += libzmq

SOURCES += \
        main.cpp

HEADERS += \
    zhelpers.hpp

LIBS = -lpthread
