#include <iostream>
#include <thread>
#include <chrono>
#include <atomic>
#include <cstdlib>
#include <ctime>
#include <memory>
#include <string>
#include <stdexcept>

#include "zhelpers.hpp"

using namespace std;

atomic_bool canContinue {true};
atomic_ulong totalRetries {0};
atomic_ulong totalRepeats {0};

template<typename ... Args>
inline string string_format( const string& format, Args ... args )
{
    size_t size = snprintf( nullptr, 0, format.c_str(), args ... ) + 1; // Extra space for '\0'
    if( size <= 0 ){ throw std::runtime_error( "Error during formatting." ); }
    unique_ptr<char[]> buf( new char[ size ] );
    snprintf( buf.get(), size, format.c_str(), args ... );
    return string( buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside
}

inline static void clientSocketSetup (char serverAddr[], const unique_ptr<zmq::socket_t>& client, const int threadNr)
{
    constexpr auto clientPortBase {34343};
    cout << "IC" << threadNr <<": connecting to server..." << endl;
    auto const connectStr {"tcp://" + string(serverAddr) + ":" + to_string(clientPortBase + threadNr)};
    client->connect (connectStr);
    cout << "IC" <<threadNr << ": connected to server " << connectStr << endl;
    //  Configure socket to not wait at close time
    int linger = 0;
    client->setsockopt (ZMQ_LINGER, &linger, sizeof (linger));
}

void reqClient (char* clientPrefix, const int threadNr, char serverAddr[])
{
    constexpr auto requestTimeout {2000};
    zmq::context_t context (1);
    auto client = make_unique<zmq::socket_t>(context, ZMQ_REQ);
    clientSocketSetup (serverAddr, client, threadNr);
    int seqNr {0};
    while (canContinue) {
        stringstream requestToSrv;
        seqNr=(++seqNr)%30000;
        requestToSrv << string_format("%5u", seqNr) << clientPrefix << seqNr;
        s_send (*client, requestToSrv.str());
        cout << "REQ" << threadNr << ">>>> : " << requestToSrv.str() << endl;
        bool expect_reply = true;
        while (expect_reply && canContinue)
        {
            //  Poll socket for a reply, with timeout
            zmq::pollitem_t items[] = {
                { static_cast<void*>(*client), 0, ZMQ_POLLIN, 0 } };
            zmq::poll (&items[0], 1, requestTimeout);
            if (items[0].revents & ZMQ_POLLIN)
            {
                //  We got a reply from the server, must match sequence
                string reply = s_recv (*client);
                //cout << "REQ: server replied OK (" << reply << ")" << endl;
                expect_reply = false;
            }
            else {
                ++totalRetries;
                cout << "W: no response from server, retrying..." << endl;
                //  Old socket will be confused; close it and open a new one
                client.reset(new zmq::socket_t(context,ZMQ_REQ));
                clientSocketSetup (serverAddr, client, threadNr);
                //  Send request again, on new socket
                s_send (*client, requestToSrv.str());
                cout << "REQ" << threadNr << ": " << requestToSrv.str() << endl;
            }
        }
        this_thread::sleep_for(chrono::milliseconds(rand()%2000)); // задержка между командами
    }
}

void repServer (int threadNr)
{
    constexpr auto serverPortBase {23232};
    zmq::context_t context(1);
    zmq::socket_t server(context, ZMQ_REP);
    server.bind("tcp://*:" + to_string(serverPortBase + threadNr));
    cout << "REP" << threadNr << ": Started server " << "tcp://*:" + to_string(serverPortBase + threadNr) << endl;
    int prevSeqNr;
    int seqNr {-1};
    while (canContinue)
    {
        zmq::pollitem_t items[] = {
            { static_cast<void*>(server), 0, ZMQ_POLLIN, 0 } };
        zmq::poll (&items[0], 1, 500); //ждать 500мс.
        if (items[0].revents & ZMQ_POLLIN)
        {
            auto replyFromSrv = s_recv (server);
            s_send (server, "OK " + replyFromSrv);
            prevSeqNr = seqNr;
            seqNr = stoi(replyFromSrv.substr(0, 5));
            auto replyText = replyFromSrv.substr(5);
            if (seqNr != prevSeqNr)
                cout << "REP" << threadNr << "<<<< : " << seqNr << " " << replyText << endl;
            else
            {
                ++totalRepeats;
                cout << "Повтор! REP" << threadNr << "<<<< : " << seqNr << " " << replyText << endl;
            }
        }
    }
}

int main(int argc, char *argv[])
/*
1 параметр: метка клиента
2 параметр: номер клиента (сдвиг относительно clientPortBase и serverPortBase)
3 параметр: адрес серевра
*/

{
    const int portShift = atoi(argv[2]);
    srand(time(nullptr));
    thread clientThread (reqClient, argv[1], portShift, argv[3]);
    thread serverThread (repServer, portShift);
    cout << "<Enter> для завершения" << endl;
    getc(stdin);
    canContinue = false;
    cout << "Waiting for finish REQ" << endl;
    clientThread.join();
    cout << "Waiting for finish REP" << endl;
    serverThread.join();
    cout << "Total reconnects: " << totalRetries << endl;
    cout << "Total repeats: " << totalRepeats << endl;
    return 0;
}
